﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour {
    int guess = 500;
    int lowestNumber = 1;
    int highestNumber = 1000;
    // Use this for initialization
    void Start () {
        StartGame();
    }

    // Update is called once per frame
    void Update () {
		if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            lowestNumber = guess;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            highestNumber = guess;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("I guessed it right!!!!");
            StartGame();
        }
    }

    void StartGame()
    {
        highestNumber = 1000;
        lowestNumber = 1;
        guess = 500;

        Debug.Log("Welcome to Number Wizard Console");
        Debug.Log("Please pick a number.");
        Debug.Log("The lowest number could be: " + lowestNumber + ".");
        Debug.Log("And the highest number could be: " + highestNumber + ".");
        Debug.Log("Tell me if your number is higher or lower than " + guess);
        Debug.Log("Push Up = Higher, Push Down = Lower, Push Enter = Correct");
        highestNumber++;
    }

    void NextGuess()
    {
        guess = (highestNumber + lowestNumber) / 2;
        Debug.Log("Is it higher or lower than... " + guess);
    }
}
